---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

![Digital Garden Lab visualization example](/assets/img/dgl-render.png)

**_Digital Garden Lab_ is an artist-led open-source community research laboratory exploring new forms of digital augmentation to facilitate urban community gardening and creation of edible urban landscapes.**

_Digital Garden Lab_ will undertake collaborative research and create bridges between community stakeholders, designers, and experts from diverse fields of knowledge. Together we will research new methods of urban food production and linked community engagement.

We will develop:

*   Software and hardware tools for intelligent gardening through a process of artistic research
*   Platform for discussion and consultation with the public, urban planners, architects, and city authorities
*   Collaborative experimentation with hacker groups and coders
*   A network of horticultural experts and community gardeners
*   Community events, exhibitions about our research and skill sharing days

Our areas of research:

*   Site data capture: GIS data, digital mapping, computer vision, photogrammetry
*   Digital modelling and algorithmic training for complex polyculture design
*   Community gardening incentivization—crypto-economics and gamification
*   Wearable tech for capturing garden metrics and facilitating human/plant interactions
*   Intelligent digital systems for management of cooperative labour
*   Augmented Reality for visualization and management of complex polycultures
*   Appropriate scale automation, garden scale robotics, and the re-valorization of human agricultural labour (‘digital Taylorism’)

Outcomes:

*   Beta versions of hardware and software
*   Community engagement program
*   Art works and exhibitions

## Why?

Imagine every green space in the city as a productive low-maintenance garden. Imagine every sidewalk, square, and park filled with fruit and nut trees, vegetables, and fragrant herbs ready for communities to harvest and share—**an abundant and intricate system of self-regulating plant growth** supporting insects, birds, animals, and people.

Permaculture, forest gardening and other forms of agroecology can do this. These systems of horticulture are designed to self-regulate. They build inherent stability through interspecies dependency. Individual plants and insects provide ‘ecological services’ to each other, and therefore to the garden itself, resulting in low maintenance productivity.

Sounds great! But designing and managing successful interspecies polycultures is complex.

There is a lack of experts who can design such systems. Detailed management is required during propagation and establishment, and ongoing targeted micro-scale care needs to happen at critical moments.

This is too expensive for city authorities to implement city wide. Budgets force them to choose planting options for urban parks that are cheap to design, implement, and maintain.

This leads to non-edible urban green space with low-biodiversity.

_DGL_ will research how digital technology can remove these limits, exploring digital interfaces between plants and people, digitally augmented **self-sustaining urban biomes with their own agency**, and intelligent software systems for managing and supporting community gardening groups and networks.


We believe this will encourage new forms of engagement with the urban landscape and increase sustainable production of food within the city itself. Secondary effects will emerge: **self-organising communities** and new food related micro businesses, wellbeing through collective work outdoors, increased urban biodiversity, and new conceptions of **mutualism between humans and non-humans** within in the urban environment.

### Appropriate accelerationism:

A lot of attention has recently focused on urban vertical farming—hi-tech hydroponic growing systems sealed off from the environment and communities. While these systems claim ecological benefits and efficiency, many of the claims are questionable, and there are many other ways to grow food in the city.

In their book _Inventing the Future_ (Verso 2015) the accelerationist theoreticians Nick Srnicek and Alex Williams offer a valuable critique of populist movements towards localised lowimpact food production, saying they are unscalable and limited by underlying ‘horizontalist folk politics’.

We believe that unlocking the potential for community-managed edible urban landscapes on large scales is an algorithmic problem. The digital can increase the scalability of horizontal urban food systems by closing knowledge gaps and facilitating management of human labour. This will bring the advantages of autonomous urban food production equally within the grasp of ordinary citizens in rich and poor nations – **a sustainable green future should be for the whole world, not a hi-tech first world elite**.

## How?

The _DGL_ concept developed when artist [Paul Chaney](http://www.paulchaney.co.uk/) met with [_Diffractions Collective_](https://diffractionscollective.org/about/) and [_/-\\ (a.k.a. slash dash backslash, or sdbs)_](https://sdbs.cz) in Prague — a joint group of tech theorists/coders with a long term interest in urban horticulture. Paul has 20 years of experience in horticulture. He has studied many horticulture-based intentional communities and urban community gardens. He has established several horticultural/agricultural systems (including a one-hectare forest garden at _End of the World Garden_ UK) and is a consultant for several large scale agro-ecological experiments (notably Kestle Barton Gallery in UK).

He is currently researching suburban horticultural and semi-urban food systems in the postsoviet space (Ukraine, and Czech Republic). He is also reading for a practice based PhD in the UK looking at the ecological efficacy of artistic engagements with land and agriculture.

Paul’s work has explored different forms of public participation and collaborative trans disciplinary research on the borders of art and science. He understands long term engagements with land and growing systems, and the seasonality of horticulture.

_Diffractions Collective_ is a group of theorists and software/hardware developers based in Prague. The developers have many years of experience between them working professionally at high levels and on personal projects. The tech theorists bring a critical understanding and ethical consideration of post-humanism to the project.

The _/-\ sdbs_ collective organises artistic expeditions into the unknown liminalities of the 21st century. Main tools used are collaborative creativity, physical exercise, teleological debate and productive intersubjectivity in the context of Temporary Autonomous Interzones and psychospiritual damage control.

The collaboration with Paul Chaney's Digital Garden Lab is a result of _/-\ sdbs_ cross-fertilization with the _Diffractions Collective_ through a shared interest in deep green technologies, post-sustainability fragmentation, patchworking and geopsychological resilience. _Diffractions Collective_ and _/-\\ sdbs_ have also been jointly developing ideas around urban horticulture through their project _[Wyrd Patchwork](https://diffractionscollective.org/?s=wyrd+patchwork)_ — an ongoing series of workshops and seminars exploring post-capitalist collapse scenarios and alternative politico-economic systems based on cryptocurrency principles and smart contracting.

During 2019 we plan to deliver a pilot program of public engagements:

*   Gather metrics of our  trial sites — soil testing/photogrammetry survey work/digital overlays
*   Community action to establish a small test garden–working with volunteers and networking with existing groups
*   Organizing initial events/networking salons
*   Develop a pilot experiment through artistic process and research
*   Develop a dynamic web presence for the project

<div class="image-center" markdown="1">
[![BLOXhub Prize Honorary Mention 2019 banner](/assets/img/honorary-mention-2019.jpg){:class='bloxhub-banner'}](https://prix.bloxhub.org/winners-2019#digital-garden-lab-digitally-augmented-edible-urban-landscapes)
</div>

_DGL_ recently won an Honorary Mention in the Prix BLOXhub sustainable digital cities ‘concept’ category (Copenhagen 2019).

## Links:

- [http://www.paulchaney.co.uk/](http://www.paulchaney.co.uk/)
- [http://eotwgarden.org.uk/](http://eotwgarden.org.uk/)
- [https://diffractionscollective.org/](https://diffractionscollective.org/)
- [https://sdbs.cz](https://sdbs.cz)
